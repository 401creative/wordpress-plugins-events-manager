#!/bin/bash

cat "../../../../gulpfile.js" | while read line
do
    echo "$line" | grep -q "var wordpress_theme"
    if [ $? -eq 0 ];then
        match=false;
        [[ "$line" =~ \'(.*)\' ]] && match=${BASH_REMATCH[0]}
        if [ $match != false ];then
            theme=${match#"'"}
            theme=${theme%"'"}

            cp -r * "../$theme";
            cd ..;
            rm -rf wordpress-events-manager;
        fi
    fi
done