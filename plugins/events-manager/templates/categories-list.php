<?php
/*
 * Default Categories List Template
 * This page displays a list of locations, called during the em_content() if this is an events list page.
 * You can override the default display settings pages by copying this file to yourthemefolder/plugins/events-manager/templates/ and modifying it however you need.
 * You can display locations (or whatever) however you wish, there are a few variables made available to you:
 * 
 * $args - the args passed onto EM_Categories::output()
 * 
 */ 

// echo EM_Categories::output( $args );
// THIS IS FROM MORPHY - REMOVE IF NOT NEEDED
global $post, $wp_query;

$args = array( 
    'taxonomy'     => 'event-categories',
    'hide_empty'   => 0,
    'orderby'      => 'name',
    'order'        => 'ASC',
);
 
$terms = get_terms( $args );
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
    echo '<div class="division-list">';
        foreach ( $terms as $term ) {
            echo '<div class="division">';
            echo '<a href="hotspot" href="'.esc_url(get_term_link($term)).'" alt="'.$term->name.'"></a>';

            $EM_Category = em_get_category($term->term_id);
            $imagesrc = wp_get_attachment_image_src($EM_Category->get_image_id(),'cta-home-two');
            echo '<img src="'.$imagesrc[0].'" class="" />';
            echo '</div>';
        }
    echo '</div>';
}