<?php
/*
 * This page displays a single event, called during the em_content() if this is an event page.
 * You can override the default display settings pages by copying this file to yourthemefolder/plugins/events-manager/templates/ and modifying it however you need.
 * You can display events however you wish, there are a few variables made available to you:
 * 
 * $args - the args passed onto EM_Events::output() 
 */
global $EM_Category, $post;
/* @var $EM_Category EM_Category */


// THIS IS FROM MORPHY - REMOVE IF NOT NEEDED
$args = array(
    'post_type' => 'event',
    'event-categories' => $EM_Category->slug,
    'posts_per_page' => -1,
    'meta_query' => array( 
        'key' => '_end_ts', 
        'value' => current_time('timestamp'), 
        'compare' => '>=', 
        'type'=>'numeric' 
    ),
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'meta_key' => '_start_ts',
    'meta_value' => current_time('timestamp'),
    // 'meta_value_num' => current_time('timestamp'),
    'meta_compare' => '!='
);

$event_list = new WP_Query( $args );

echo '<div class="calendar-wrap clearfix">';
    echo '<div class="list clearfix">';

if( $event_list->have_posts() ) :

    while( $event_list->have_posts() ) : $event_list->the_post();
        
        $GLOBALS['isdivision'] = true;
        get_template_part('templates/loop', 'auction');
        $GLOBALS['isdivision'] = false;

    endwhile; else : echo '<p class="no-auctions">No Auctions/Events, check back later</p>';

wp_reset_postdata(); endif;
    echo '</div>';
echo '</div>';
