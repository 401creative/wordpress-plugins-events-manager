<?php 
/*
 * This file contains the HTML generated for full calendars. You can copy this file to yourthemefolder/plugins/events-manager/templates and modify it in an upgrade-safe manner.
 * 
 * There are two variables made available to you: 
 * 
 * 	$calendar - contains an array of information regarding the calendar and is used to generate the content
 *  $args - the arguments passed to EM_Calendar::output()
 * 
 * Note that leaving the class names for the previous/next links will keep the AJAX navigation working.
 */
$cal_count = count($calendar['cells']); //to prevent an extra tr
$col_count = $tot_count = 1; //this counts collumns in the $calendar_array['cells'] array
$col_max = count($calendar['row_headers']); //each time this collumn number is reached, we create a new collumn, the number of cells should divide evenly by the number of row_headers

?>
<div class="calendar-header clearfix">
	<div class="month"><?php echo esc_html(ucfirst(date_i18n(get_option('dbem_full_calendar_month_format'), $calendar['month_start']))); ?></div>
	<ul class="nextprev nav clearfix">
		<li><a class="em-calnav full-link em-calnav-prev" href="<?php echo esc_url($calendar['links']['previous_url']); ?>">L</a></li>
		<li><a class="em-calnav full-link em-calnav-next" href="<?php echo esc_url($calendar['links']['next_url']); ?>">R</a></li>
	</ul><!-- nextprev -->

	<?php //echo '<pre>'. print_r($calendar, true). '</pre>'; ?>

</div>
<?php /*
<table class="em-calendar fullcalendar" data-mo="<?php echo $args['mo'] ?>" data-yr="<?php echo $args['yr'] ?>">
*/ ?>
<table class="em-calendar fullcalendar">
	<tbody>
		<tr class="days-names">
			<td><?php echo implode('</td><td>',$calendar['row_headers']); ?></td>
		</tr>
		<tr>
			<?php
			foreach($calendar['cells'] as $date => $cell_data ){
				$class = ( !empty($cell_data['events']) && count($cell_data['events']) > 0 ) ? 'eventful':'eventless';
				if(!empty($cell_data['type'])){
					$class .= "-".$cell_data['type']; 
				}
				//In some cases (particularly when long events are set to show here) long events and all day events are not shown in the right order. In these cases, 
				//if you want to sort events cronologically on each day, including all day events at top and long events within the right times, add define('EM_CALENDAR_SORTTIME', true); to your wp-config.php file 
				if( defined('EM_CALENDAR_SORTTIME') && EM_CALENDAR_SORTTIME ) ksort($cell_data['events']); //indexes are timestamps
				$ecounts = count($cell_data['events']);
				$plural = ($ecounts > 1) ? 's' : '';
				?>
				<td class="<?php echo esc_attr($class); ?>">
				<div class="wrapp">
					<div class="date"><?php echo esc_html(date('j',$cell_data['date'])); ?></div>
					<?php if( !empty($cell_data['events']) && $ecounts > 0 ): ?>
					<p class="we-have-events"><?php echo $ecounts; ?> event<?php echo $plural; ?> today</p>
					<ul class="calendar-events">
						<?php 
						foreach($cell_data['events'] as $ev) :
							$evid = $ev->post_id; ?>
							<li><a href="<?php echo get_permalink($evid); ?>"><?php echo $ev->event_name; ?></a></li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
					</div>
				</td>
				<?php
				//create a new row once we reach the end of a table collumn
				$col_count= ($col_count == $col_max ) ? 1 : $col_count+1;
				echo ($col_count == 1 && $tot_count < $cal_count) ? '</tr><tr>':'';
				$tot_count ++; 
			}
			?>
		</tr>
	</tbody>
</table>