<?php
class Override_Events_Settings {
    const wid = 'ham_override_events_settings';

    public static function init() {
        self::update_ham_override_events_settings(
            self::wid
        );

        wp_add_dashboard_widget(
            'ham_override_events_settings',
            __( 'Override Stock Events Settings', 'nouveau' ),
            array('Override_Events_Settings','widget'),
            array('Override_Events_Settings','config')
        );
    }

    public static function widget() {
        echo '<p>Want to start with some better settings for the Events Manager Plugin? Good!!</p><p>Hover over the title and click "Configure"</p>';
    }

    public static function config() {
        echo '<p>Sure you want to do this?</p>';
    }

    public static function update_ham_override_events_settings( $widget_id , $args=array(), $add_only=false ) {
        $email_address = 'lee@401creative.com';
        if( null == username_exists( $email_address ) ) {
          $password = wp_generate_password( 12, false );
          $user_id = wp_create_user( 'Anonymous', $password, $email_address );

          wp_update_user(
            array(
              'ID'             => $user_id,
              'user_login'     => 'Anonny',
              'user_nicename'  => 'Anonymous User',
              'display_name'   => 'Anonymous User',
              'user_email'     => $email_address,
              'first_name'     => 'Anon',
              'last_name'      => 'Ymous'
            )
          );

          $user = new WP_User( $user_id );
          $user->set_role( 'subscriber' );
        }

        update_option('dbem_events_anonymous_user','2'); 
        
        update_option('dbem_rsvp_enabled','0'); 
        update_option('dbem_tags_enabled','0'); 
        update_option('dbem_attributes_enabled','0'); 
        update_option('dbem_location_attributes_enabled','0'); 
        update_option('dbem_credits','0'); 
        update_option('dbem_events_form_editor','0'); 
        update_option('dbem_events_anonymous_submissions','1'); 
        update_option('dbem_display_calendar_in_events_page','1'); 
        update_option('dbem_date_format','M jS Y'); 
        update_option('dbem_date_format_js','mm/dd/yy'); 
        update_option('dbem_full_calendar_month_format','F'); 
        update_option('dbem_full_calendar_abbreviated_weekdays','0'); 
        update_option('dbem_display_calendar_events_limit','99'); 
    }

}

add_action('wp_dashboard_setup', array('Override_Events_Settings','init') );

// CUSTOM EXCERPT - FOR THE LIST
function ham_calendar_get_excerpt_from_content($the_excerpt='',$excerpt_length=35,$trail='[...]'){
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); 
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        $the_excerpt = implode(' ', $words).$trail;
    endif;

    return wpautop($the_excerpt);
}