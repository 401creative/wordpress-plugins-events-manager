<?php get_header(); ?>
        
    <?php get_header_images(); ?>

    <div class="content container clearfix">

        <?php 
        if (function_exists('yoast_breadcrumb')) : yoast_breadcrumb('<p class="crumbs">','</p>'); endif; 

        if(have_posts()): while(have_posts()): the_post(); ?>
           
            <div class="left">

                <div class="single-datetime">
                    <span class="icon-date"><?php echo $EM_Event->output('#_EVENTDATES'); ?></span>
                    <span class="icon-time"><?php echo $EM_Event->output('#_EVENTTIMES'); ?></span>
                </div>

                <?php if(has_post_thumbnail()) echo get_the_post_thumbnail(get_the_ID(),'medium', array('class'=>'alignleft')); 
                ?>

                <?php echo $EM_Event->output('#_EVENTNOTES'); ?>

            </div><!-- left -->
                
            <div class="sidebar">
                <?php dynamic_sidebar('primary'); ?>
            </div><!-- sidebar -->
    
        <?php endwhile; endif; ?>
    
    </div><!-- content -->

<?php get_footer(); ?>