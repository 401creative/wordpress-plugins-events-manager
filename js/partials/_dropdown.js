;(function($) {

    $.dropdownreplace = function(element, options) {

        var $element    = $(element),
            select      = null,
            dropdown    = $('<div class="dropdown hidden"></div>'),
            current     = $('<span class="current"/>'),
            listwrap    = $('<div class="selection">'),
            list        = $('<ul class="selection-list">'),
            item        = $('<li/>'),
            list_items  = [];

        var defaults = {
            onChangeSelect: function(val) {}
        }

        var plugin = this;

        plugin.settings = {}

        plugin.init = function() {
            plugin.settings = $.extend({}, defaults, options);
            select = $element.find('select');
            hideSelect();
            addMarkup();
            setupFocus();
            setupEvents();
        }
        
        var hideSelect = function(){
            select.css({position:'absolute',top:0,left:-9999});
        }

        var addMarkup = function(){
            select.find('option').each(function(){
                var new_li = item.clone(),
                    the_class = $(this).attr('class');
                new_li.html( $(this).html() ).data('value',$(this).val()).data('filter',$(this).data('filter'));
                    new_li.addClass(the_class);
                if( $(this).is(':selected') ) {
                    var name = $(this).html();
                    if( $(this).data('altname') ) name = $(this).data('altname');
                    current.html( '<strong>'+name+'</strong>' );
                    new_li.addClass('current').data('altname',name);
                }
                list.append(new_li);
                listwrap.append(list);
                list_items.push(new_li);
            });
            dropdown.append(current).append(listwrap);
            select.after( dropdown );
        }

        var setupFocus = function(){
            select.on('focus',function(){
                dropdown.removeClass('hidden');
            }).on('blur',function(){
                var s = this;
                setTimeout(function(){
                    if( !$(s).is(':focus') ) dropdown.addClass('hidden');
                },200);
            });
        }

        var setupEvents = function(){
            for( i in list_items ){
                var li = list_items[i];
                li.on('click',function(e){
                    e.preventDefault();
                    var val = $(this).data('value');
                    select.val( val ).trigger('change');
                });
            }
            select.on('change',function(){
                var _val = $(this).val();
                for( i in list_items ){
                    var li = list_items[i];
                    var val = li.data('value');
                    if( val === _val ) {
                        changeSelect($(li));
                    }
                }
            });
        }

        var changeSelect = function(li){
            var val = li.data('value');
            li.addClass('current').siblings().removeClass('current');
            var name = li.html();
            if( li.data('altname') ) name = li.data('altname');
            current.html( '<strong>'+name+'</strong>' );
            plugin.settings.onChangeSelect($element,val);
        }

        plugin.init();

    }

    $.fn.dropdownreplace = function(options) {

        return this.each(function() {
            if (undefined == $(this).data('dropdownreplace')) {
                var plugin = new $.dropdownreplace(this, options);
                $(this).data('dropdownreplace', plugin);
            }
        });

    }

})(jQuery);