;(function($) {

    $.blogFeed = function(element, options) {

        var $element = $(element),
             element = element;

        var defaults = {
            moreButton: $('.more-btn'),
            //infiniteScroll: 'always', // 'after' (start infinite scroll after more button is clicked once), 'always' (start infinite scroll on page load), 'never' (never start infinite scroll)
            appendOrReplace: 'append', // 'append' (append new articles to list), 'replace' (replace current list with new articles) *** only valid if infiniteScroll equals 'never' ***
            onBeforeAjax: function() {},
            onAfterAjax: function() {},
            onBeforeContentChange: function(html) {},
            onAfterContentChange: function(html) {}
        }

        var blogFeed = this;

        var blogFeedId = $element.attr('id');
        var maxPages = $element.data('maxpages');
        var currentPage = $element.data('currentpage');
        var nextPage = $element.data('nextpage');

        blogFeed.settings = {}

        blogFeed.init = function() {
            blogFeed.settings = $.extend({}, defaults, options);
            if( blogFeed.settings.infiniteScroll === 'always' ) {
                blogFeed.settings.moreButton.remove();
            } else {
                setupMoreButton();
            }
        }

        blogFeed.loadNextPage = function() {
            blogFeed.loadPage( nextPage );
        }

        blogFeed.loadPage = function(url,appendOrReplace) {
            blogFeed.settings.onBeforeAjax();
            appendOrReplace = appendOrReplace ? appendOrReplace : blogFeed.settings.appendOrReplace;
            $.ajax({
                url: url,
                type: 'POST',
                data: 'html'
            }).success(function(html){
                blogFeed.settings.onBeforeContentChange(html);
                var $newElement = $(html).find('#'+blogFeedId);
                maxPages = $newElement.data('maxpages');
                currentPage = $newElement.data('currentpage');
                nextPage = $newElement.data('nextpage');
                if( appendOrReplace === "replace" ) {
                    $element.empty();
                }
                $element.append( $newElement.contents() );
                if( currentPage >= maxPages ) {
                    blogFeed.settings.moreButton.hide();
                } else {
                    blogFeed.settings.moreButton.show();
                }
                blogFeed.settings.onAfterContentChange(html);
                blogFeed.settings.onAfterAjax();
            });
        }

        var setupMoreButton = function() {
            blogFeed.settings.moreButton.on('click',function(e){
                e.preventDefault();
                blogFeed.loadNextPage();
            });
        }

        blogFeed.init();

    }

    $.fn.blogFeed = function(options) {

        return this.each(function() {
            if (undefined == $(this).data('blogFeed')) {
                var blogFeed = new $.blogFeed(this, options);
                $(this).data('blogFeed', blogFeed);
            }
        });

    }

})(jQuery);