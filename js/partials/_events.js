jQuery(document).on('wpfc_fullcalendar_args',function(e,a){
    a.eventRender = customEventRender;
    a.eventAfterAllRender = afterRender;
});

jQuery(window).on('resize',function(){
    checkCalLayout();
});

function customEventRender(event, element, view){
    element.attr('data-date',event.start._i.substr(0,10));
    return element;
}

function checkCalLayout(){
    if(jQuery(window).width() < 760){
        jQuery('.fc-agendaDay-button').trigger('click');
        jQuery('.event-popup-trigger').remove();
        jQuery('.events-popup-container').remove();
        return true;
    } else {
        return false;
    }
}

function afterRender(a){
    if(checkCalLayout())return;

    if(a.intervalUnit == "month"){
        // View has finished rendering, and is in month view.

        var days = {};
        jQuery('.fc-day-grid-event').each(function(){
            var date = jQuery(this).data('date');
            if(!days[date])days[date] = {events:[]};
            days[date].events.push(jQuery(this).clone());
        });

        for(var date in days){
            if(days[date].events.length){
                var day_element = jQuery('.fc-day[data-date="'+date+'"]');
                day_element.append('<p class="event-popup-trigger">'+days[date].events.length+' Events</p>');
                var container = jQuery('<div class="events-popup-container"></div>');
                var element = jQuery('<div class="events-popup"></div>');
                for(var i in days[date].events){
                    element.append(days[date].events[i]);
                }
                jQuery(container).append(element);
                day_element.append(container);
            }
        }
    }
}


jQuery(function($){
    $('#calendarList').blogFeed({
        moreButton: $('.load-more'),
        onBeforeAjax: function(){
            $('.load-more').addClass('loading');
        },
        onAfterAjax: function(){
            $('#calendarList').find('loading').remove();
            $('.load-more').removeClass('loading');
        }
    });

    $( '#event_image' ).each( function() {
        var $input   = $( this ),
            $label   = $input.closest('.gfield').find( 'label' ),
            labelVal = $label.html();

        $input.on( 'change', function( e ) {
            var fileName = '';

            if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                $label.find( 'span' ).html( fileName );
            else
                $label.html( labelVal );
        });

        // Firefox bug fix
        $input
        .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });
    
    $(this).find('.filter-wrap .dropdownreplace').dropdownreplace();
    
    $('.dropdown-btn').click(function(e) {
        e.stopPropagation();
        $(this).parent().toggleClass('open');
    });
    
    $('html').click(function(e) {
        $('.dropdownreplace').removeClass('open');
    });

    $('.calendar-toggle .toggle_calendar').on('click',function(e) {
        e.preventDefault();
        $container = $(this).closest('.content').find('.calendar-wrap');

        $(this).toggleClass('on').siblings().removeClass('on');
        $container.find('.views').filter('.hidden').toggleClass('hidden').siblings().toggleClass('hidden');
    });
    
    var event_filter = {
        mo: false,
        yr: false,
        category: false
    };

    $(document).on('click', '.dd-event .selection a', function(e) {
        e.preventDefault();
        if( $(this).data('cat') ) event_filter.category = $(this).data('cat')=='all' ? false : $(this).data('cat');
        if( $('.em-calendar').data('mo') ) event_filter.mo = $('.em-calendar').data('mo');
        if( $('.em-calendar').data('yr') ) event_filter.yr = $('.em-calendar').data('yr');
        $(this).closest('.dropdownreplace').find('.dropdown-btn').text($(this).data('title') || $(this).text());
        $('.em-calendar-wrapper').prepend('<div class="loading" id="em-loading"></div>');
        var url = $('.em-calendar-wrapper .em-calnav-next').attr('href');
        url = url.replace( /\&category=\d+|\&mo=\d+|\&yr=\d+/ig, '' );
        for( i in event_filter ) {
            if( event_filter[i] ) url += '&'+i+'='+event_filter[i];
        }
        url = em_ajaxify(url);
        $('.em-calendar-wrapper').load(url, function(){$(this).trigger('em_calendar_load');});
        var blogFeed = $('#calendarList').data('blogFeed');
        bfurl = $('#calendarList').data('baseurl');
        var q = true;
        for( i in event_filter ) {
            if( event_filter[i] ) {
                bfurl += (q?'?':'&')+i+'='+event_filter[i];
                q = false;
            }
        }
        // console.log(bfurl);
        $('#calendarList').prepend($('<div class="loading" id="em-loading"></div>'));
        blogFeed.loadPage(bfurl,'replace');
    });
});