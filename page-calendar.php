<?php 
/*
Template Name: Calendar
*/
get_header(); ?>

    <?php get_header_images(); ?>
        
    <div class="content container clearfix">
        
     <?php 
        if (function_exists('yoast_breadcrumb')) : yoast_breadcrumb('<p class="crumbs">','</p>'); endif;

        // INTRO TEXT CAN GO HERE - MOST LIKELY A WYSIWYG FROM SITE-OPTIONS

        ?>

        <div class="calendar-filter clearfix">

            <div class="dropdownreplace dd-event category" id="select-category">
                <div class="selection">
                  <ul class="selection-list">
                      <?php $cats = get_terms( 'event-categories', array( 'orderby' => 'term_order', 'hide_empty' => false ) ); ?>
                      <li><a href="javascript:;" data-cat="all" data-title="Filter by Category">All Categories</a></li>
                      <?php
                      $category_title = 'Filter by Category';
                      foreach( $cats as $cat ) {
                        if( !empty($_GET['category']) && $_GET['category']==$cat->term_id ) $category_title = $cat->name;
                        echo '<li><a href="javascript:;" data-cat="'.$cat->term_id.'">'.$cat->name.'</a></li>';
                      }
                      ?>
                  </ul><!-- category -->
                </div>
                <a href="javascript:;" class="dropdown-btn current" id="btn_select_category"><?php echo $category_title; ?></a>
            </div>

            <?php
            if( !get_query_var('filtered') ) :
              ?>
              <div class="calendar-toggle clearfix">
                  <p>View</p>
                  <button class="toggle_calendar calendar on"></button>
                  <button class="toggle_calendar list"></button>
              </div>
              <?php
            endif;
            ?>

            <a class="btn submit-an-event" href="<?php echo site_url( '/visit-bethlehem/event-calendar/submit-your-event' ); ?>">Submit Your Event</a>
        </div><!-- calendar filter -->

        <div class="calendar-wrap clearfix">

            <?php
            if( !get_query_var('filtered') ) :
              ?>
                <div class="views calendar clearfix">
                <?php if(have_posts()): while(have_posts()): the_post(); 
                    $subtitle = get_post_meta(get_the_ID(), '_ham_custom_page_custom_subheading',true);
                    if($subtitle) echo '<h2 class="subtitle">'.$subtitle.'</h2>';
                    the_content();
                endwhile; endif; ?>
               </div>
              <?php
            endif;
            include('templates/calendar-list.php'); ?>
        </div><!-- calendar wrap -->
    
    </div><!-- content -->

<?php get_footer(); ?>