<?php 
global $post, $paged, $EM_Event;

$type = "";

$args = array(
    'post_type' => 'event',
    'paged' => get_query_var('paged'),
    'meta_query' => array( 
        'key' => '_end_ts', 
        'value' => current_time('timestamp'), 
        'compare' => '>=', 
        'type'=>'numeric' 
    ),
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'meta_key' => '_start_ts',
    'meta_value' => current_time('timestamp'),
    'meta_compare' => '!='
);
if(isset($_GET['category'])) {
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'event-categories',
            'field'    => 'term_id',
            'terms'    => floatval($_GET['category']),
        ),
    );
}

$event_list = new WP_Query( $args );

?>
<div class="views list <?php echo !get_query_var('filtered') ? 'hidden' : ''; ?> clearfix">
    <?php
    if( $event_list->have_posts() ) :

        $maxPages = $event_list->max_num_pages;
        $currentPage = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
        $nextPage = next_posts( $maxPages, false );
        ?>
        <div id="calendarList" data-baseurl="<?php echo get_post_type_archive_link( 'event' ); ?>" data-maxpages="<?php echo $maxPages; ?>" data-currentpage="<?php echo $currentPage; ?>" data-nextpage="<?php echo $nextPage; ?>">

          <?php  
          while( $event_list->have_posts() ) : $event_list->the_post();

            $EM_Event = em_get_event(get_the_ID(), 'post_id');
                
            $startts = get_post_meta(get_the_ID(), '_start_ts', true);
            $endts = get_post_meta(get_the_ID(), '_end_ts', true);
            $starttime = get_post_meta(get_the_ID(), '_event_start_time', true);
            $start_date = get_post_meta(get_the_ID(), '_event_start_date',true);
            $end_date = get_post_meta(get_the_ID(), '_event_end_date',true);

            ?>
            <div class="listed-event clearfix">
                <?php
                $link = get_permalink();
                if(has_post_thumbnail()) {
                    $thumb_id = get_post_thumbnail_id(get_the_ID());
                    $image_output_src = wp_get_attachment_image_src( $thumb_id, 'full', false );
                    $image_output = '<img src="'.aq_resize( $image_output_src[0], 380, 230, true ).'" />';
                    echo '<a href="'.$link.'">'.$image_output.'</a>';
                } else {
                    echo '<a href="'.$link.'"><img src="'.get_stylesheet_directory_uri().'/images/default-list.jpg" /></a>';
                }

                ?>
                <div class="text">
                    <h4><?php 
                        $eventcats = get_the_terms( get_the_ID() , 'event-categories' );
                        echo $eventcats[0]->name;
                    ?></h4>
                    <h3><?php the_title(); ?></h3>
                    <div class="date">
                    <?php
                        if($start_date == $end_date) {
                            echo date('F d, Y', $startts); 
                        } else {
                            echo date('F d', $startts).' - '. date('d, Y', $endts);
                        }
                    ?>,
                        <span><?php echo ucfirst($type); ?> Begins at <?php echo date('g:i A', strtotime($starttime)); ?></span>
                    </div>

                    <?php
                        echo ham_calendar_get_excerpt_from_content($EM_Event->output( "#_EVENTEXCERPT" ),30,'&hellip; </p><a href="'.$link.'" class="more">More Info ></a>');
                    ?>
                </div><!-- text -->
            </div><!-- listed event -->

          <?php endwhile; ?>

        </div><!-- list -->

        <?php if( $maxPages > $currentPage ) : ?>
          <a class="load-more btn primary-btn" href="<?php echo next_posts( $maxPages, false ); ?>">View More</a>
        <?php endif; ?>

        <?php
        else : echo '<div id="calendarList" class="list"><p class="no-events">No Events, check back later</p></div>';

        ?>

    <?php 
    endif; 

    // Reset Post Data
    wp_reset_postdata();
    ?>
</div>